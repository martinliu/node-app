import logo from './devops.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Learn DevOps and run faster 🏃🏻‍♀️🏃🏻‍♀️🏃🏻‍♀️🏃🏻‍♀️🏃🏻‍♀️🏃🏻‍♀️.
        </p>
        <a
          className="App-link"
          href="https://devopschina.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          DevOps China community!
        </a>
      </header>
    </div>
  );
}

export default App;
